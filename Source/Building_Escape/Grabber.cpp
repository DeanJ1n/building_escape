// Fill out your copyright notice in the Description page of Project Settings.

#include "Grabber.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/Actor.h"
#include "DrawDebugHelpers.h"
#include "Collision.h"

//this doesnt do anything that changes the code, but it makes the code more understandable
#define OUT

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	//print the following line to indicate grabber working in UE4 console
	UE_LOG(LogTemp, Warning, TEXT("Grabber repoting for duty"));

	//get the player controller of the pawn owning this object
	controller = GetWorld()->GetFirstPlayerController();
	FindPhysicsHandleComponent();
	SetupInputComponent();
}

void UGrabber::SetupInputComponent()
{
	//look for input component
	inputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
	//check if input component is found
	if (inputComponent) {
		//if the input component is found, bind actions with functions
		inputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
		inputComponent->BindAction("Grab", IE_Released, this, &UGrabber::Release);
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("INPUT COMPONENT MISSING ON %s"), *GetOwner()->GetName());
	}
}

void UGrabber::FindPhysicsHandleComponent()
{
	//look for attached physics handle
	physicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	//check if physics handle is found
	if (physicsHandle) {
		//physics handle is found
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("PHYSICS HANDLE MISSING ON %s"), *GetOwner()->GetName());
	}
}


void UGrabber::Grab() {

	//get the hit
	FHitResult hit = GetFirstPhysicsBodyInReach();
	UPrimitiveComponent* hitComponent = hit.GetComponent();

	//check if anything is hit to be grabbed
	if (hitComponent && physicsHandle) {
		//TODO attach the reached object to the pawn
		physicsHandle->GrabComponent(
			hitComponent,
			NAME_None,
			hitComponent->GetOwner()->GetActorLocation(),
			true	//allow rotation
		);
	}
}

void UGrabber::Release()
{
	//release the object if there is a object grabbed
	if (physicsHandle && physicsHandle->GrabbedComponent) {
		physicsHandle->ReleaseComponent();
	}
}

FHitResult UGrabber::GetFirstPhysicsBodyInReach() const
{
	//query parameters for input in line trace
	FCollisionQueryParams queryParams(FName(TEXT("")), false, GetOwner());

	//Line tracing (or ray casting) out to reach distance
	FHitResult hitResult;

	GetWorld()->LineTraceSingleByObjectType(
		OUT hitResult,
		ReachLineStart(),
		ReachLineEnd(),
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		queryParams
	);

	return hitResult;
}


FVector UGrabber::ReachLineStart() const
{
	//get the line trace end
	FVector location;
	FRotator rotation;

	//each tick we get the view point
	controller->GetPlayerViewPoint(OUT location, OUT rotation);

	return location;
}

FVector UGrabber::ReachLineEnd() const
{
	//get the line trace end
	FVector location;
	FRotator rotation;

	//each tick we get the view point
	controller->GetPlayerViewPoint(OUT location, OUT rotation);

	//get the end point for this ray
	return (location + rotation.Vector() * reach);
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
	if (!physicsHandle) {
		return;
	}

	//check if the physics handle has grabbed anything
	if (physicsHandle->GrabbedComponent) {

		//move the object that we're holding
		physicsHandle->SetTargetLocation(ReachLineEnd());
	}

}
