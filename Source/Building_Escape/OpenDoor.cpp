// Fill out your copyright notice in the Description page of Project Settings.

#include "OpenDoor.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "Components/PrimitiveComponent.h"

#define OUT

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();
	
	//Find the owner of this script
	owner = GetOwner();

	//check if pressure plate exists
	if (!pressurePlate) {
		UE_LOG(LogTemp, Warning, TEXT("Missing pressure plate"));
	}
}

// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// poll the trigger volume, which means to check for this status every frame
	if (GetTotalMassOfActorsOnPlate() >= triggerMass) {

		//open the door with bluprint smoothly when enough mass on trigger
		onOpenRequest.Broadcast();
	}
	else {
		onCloseRequest.Broadcast();
	}
}

float UOpenDoor::GetTotalMassOfActorsOnPlate() const
{
	float totalMass = 0.0f;

	//find all the overlapping actors
	TArray<AActor*> overlappingActors;
	if (pressurePlate) {
		pressurePlate->GetOverlappingActors(OUT overlappingActors);
	}

	//add up all the mass
	for (AActor* x : overlappingActors) {
		totalMass += x->FindComponentByClass<UPrimitiveComponent>()->GetMass();
	}

	return totalMass;
}

