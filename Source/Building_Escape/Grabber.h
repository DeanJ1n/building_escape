// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Components/InputComponent.h"
#include "Components/PrimitiveComponent.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDING_ESCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	//here are the private buffer fields of the class
	APlayerController* controller = nullptr;
	UPhysicsHandleComponent* physicsHandle = nullptr;
	UInputComponent* inputComponent = nullptr;

	//here are the design levers
	UPROPERTY(EditAnyWhere)
	float reach = 100.0f;

	//functions
	void FindPhysicsHandleComponent();
	void SetupInputComponent();
	void Grab();	//raycast and grab what's in reach
	void Release();	//happens after the key is released
	FHitResult GetFirstPhysicsBodyInReach() const;
	FVector ReachLineStart() const;	//get the start of reach line
	FVector ReachLineEnd() const;	//get the end of reach line
};
